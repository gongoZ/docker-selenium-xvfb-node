#!/bin/sh

# Environments

    export DISPLAY=:10

# Start xvfb

    /usr/bin/Xvfb :10 -screen 0 1024x768x24 -ac &

# Start selenium

    SELENIUM_HUB_PORT=${SELENIUM_HUB_PORT:-"4444"}
    SELENIUM_NODE_HOST=${SELENIUM_NODE_HOST:-"127.0.0.1"}
    SELENIUM_NODE_PORT=${SELENIUM_NODE_PORT:-"5555"}

    [ -z "${SELENIUM_HUB_HOST}" ] && exit 1

    /usr/bin/java -jar /opt/selenium/selenium-server-standalone.jar \
        -role webdriver \
        -hub  http://${SELENIUM_HUB_HOST}:${SELENIUM_HUB_PORT}/grid/register \
        -host ${SELENIUM_NODE_HOST} \
        -port ${SELENIUM_NODE_PORT}
